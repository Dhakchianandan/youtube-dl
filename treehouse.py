#parser.py
import string
import sys
import urllib
import xml.etree.ElementTree as ET

arg=sys.argv[1]
feed=arg.replace("itpc://", "http://");

def formatString(unformatted):
    unformatted=unformatted.lower()
    unformatted=unformatted.strip()
    unformatted=unformatted.replace("treehouse - ", "");
    unformatted=unformatted.replace("&", "and");
    unformatted=unformatted.translate(None, "'?()")
    unformatted=unformatted.replace("  ", " ");
    unformatted=unformatted.replace(" ", "-");
    formatted=unformatted.replace("--", "-");
    return formatted

def getList(listOfDicts, key):
    return [i[key] for i in listOfDicts if key in i]

i=0
nChapter=0
nEpisode=0
oldChapter=''
extension='.mp4'

try:
    query_params = {}
    query_params["hd"] = "true"
    encoded_query_params = urllib.urlencode(query_params)
    f=urllib.urlopen(feed + "&" + encoded_query_params)
    s=f.read()

    episode=dict()
    repository=list()

    root = ET.fromstring(s)

    for channel in root.iter('channel'):
        course=formatString(channel.find('title').text)

        for item in channel.iter('item'):
            t=item.find('title').text
            title= t.split(': ')

            chapter=formatString(title[0])
            name=formatString(title[1])

            url=item.find('enclosure').get('url')

            episode['course']=course
            if oldChapter != chapter:
                 nChapter=nChapter+1
                 nEpisode=1
            episode['chapter']=format(nChapter,"02d")+'-'+chapter
            episode['name']=format(nEpisode,"02d")+'-'+name
            episode['url']=url
            episode['filename']=episode['name']+extension

            repository.append(episode.copy())

            i+=1
            nEpisode+=1
            oldChapter=chapter

    for ep in repository:
        output='treehouse/'+ep['course']+'/'+ep['chapter']+'/'+ep['filename']
        print "curl -L --create-dirs -o "+output+" \""+ep['url'] + "\""

except(Exception) as e:
    print e