from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import re

username = ""
password = ""
YOUTUBE_DL = "youtube-dl -u " + username + " -p " + password + " -o "

driver = webdriver.Chrome()
driver.get("https://www.raywenderlich.com/session/new?mode=login&redirect_uri=https%3A%2F%2Fwww.raywenderlich.com%2F")
try:

    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "a0-signin_easy_email"))
    )

    element.send_keys(username)
    element = driver.find_element_by_id("a0-signin_easy_password")
    element.send_keys(password)
    element = driver.find_element_by_class_name("a0-next")
    element.submit()

    menu_drop_down_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "video-dropdown"))
    )

    # webdriver.ActionChains
    driver.execute_script("jQuery('.video-dropdown').toggle();")
    screencasts_link = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, "id('main-links')/ul/li[2]/ul/li[3]/a"))
    )
    screencasts_link.click()

    screencast_list = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "screencast-list"))
    )

    screencasts = screencast_list.find_elements(By.CLASS_NAME, "screencast")
    screencasts.reverse()
    screencasts_length = len(screencasts)

    for index in range(screencasts_length):
        screencast_list = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "screencast-list"))
        )

        try:
            screencast = screencasts[index]
            screencast.find_element(By.CLASS_NAME, "screencast-title")
        except(Exception) as e:
            screencasts = screencast_list.find_elements(By.CLASS_NAME, "screencast")
            screencasts.reverse()
            screencast = screencasts[index]

        screencast_link = screencast.find_element(By.CLASS_NAME, "screencast-title")
        screencast_link_text = screencast_link.text
        screencast_link_text = re.sub(":|&|-", "", screencast_link_text)
        screencast_link_text = re.sub(" ", "_", screencast_link_text)
        screencast_title = str(index + 1) + "." + screencast_link_text
        screencast_link.click()

        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "player"))
        )

        vimeo_url = driver.find_element_by_xpath("id('player')/video/source")
        print YOUTUBE_DL + screencast_title.lower() + ".mp4 " + vimeo_url.get_attribute("src")

        driver.back()

except(Exception) as e:
    print e

finally:
    time.sleep(10)
    driver.quit()
    print "END"