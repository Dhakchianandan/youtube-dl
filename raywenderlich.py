from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import re

username = ""
password = ""
YOUTUBE_DL = "youtube-dl -u " + username + " -p " + password + " -o "

driver = webdriver.Chrome()
driver.get("https://www.raywenderlich.com/session/new?mode=login&redirect_uri=https%3A%2F%2Fwww.raywenderlich.com%2F")
try:

    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "a0-signin_easy_email"))
    )

    element.send_keys(username)
    element = driver.find_element_by_id("a0-signin_easy_password")
    element.send_keys(password)
    element = driver.find_element_by_class_name("a0-next")
    element.submit()

    menu_drop_down_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "video-dropdown"))
    )

    # webdriver.ActionChains
    driver.execute_script("jQuery('.video-dropdown').toggle();")
    courses_link = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, "id('main-links')/ul/li[2]/ul/li[2]/a"))
    )
    courses_link.click()

    course_list = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "course-list"))
    )

    # for course_group in course_groups:
    #     course_group_title = course_group.get_attribute("id")
    # (By.XPATH, "id('player')/video/source/@src")

    # "id('course-list')/div/div[4]/div[i]/div[2]/a"
    course_link = driver.find_element_by_xpath("id('course-list')/div/div[7]/div[6]/div[2]/a")
    course_link.click()

    course_page_video_link_element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, "id('player')/video/source"))
    )

    # print course_page_video_link_element.get_attribute("src")
    lesson_table = driver.find_element_by_class_name("lesson-table")
    lessons = lesson_table.find_elements(By.TAG_NAME, "li")

    lessons_length = len(lessons)
    # for lesson in lessons:
    for index in range(lessons_length):

        time.sleep(1)

        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "course-lessons"))
        )

        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "player"))
        )

        try:
            lesson = lessons[index]
            lesson.find_element(By.CLASS_NAME, "lesson-number")
        except(Exception) as e:
            lesson_table = driver.find_element_by_class_name("lesson-table")
            lessons = lesson_table.find_elements(By.TAG_NAME, "li")
            lesson = lessons[index]

        vimeo_url = driver.find_element_by_xpath("id('player')/video/source")
        lesson_number = lesson.find_element(By.CLASS_NAME, "lesson-number").text
        lesson_title = lesson.find_element(By.CLASS_NAME, "lesson-name").text
        lesson_title = lesson_number + "." + re.sub("new|free", "", lesson_title, flags=re.IGNORECASE)
        lesson_title = re.sub(" ", "_", lesson_title, flags=re.IGNORECASE)
        lesson_title = re.sub("_&", "", lesson_title, flags=re.IGNORECASE)

        print YOUTUBE_DL + lesson_title.lower() + ".mp4 " + vimeo_url.get_attribute("src")
        # print vimeo_url.get_attribute("src")

        if(index + 1 < lessons_length):
            next_lesson = lessons[index + 1]
            next_lesson_link = next_lesson.find_element(By.CLASS_NAME, "lesson-link")
            # print next_lesson_link.get_attribute("href")
            next_lesson.click()


except(Exception) as e:
    print e

finally:
    time.sleep(10)
    driver.quit()
    print "END"